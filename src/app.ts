import * as express from 'express';
import { escape as escape} from 'querystring';
import * as https from 'https';
import * as fs from 'fs';

let langstr:string = '';
//const https_options ={
//    key: fs.readFileSync('/home/ole/prog/openssl-stuff/xip-io.key'),
//    cert: fs.readFileSync('/home/ole/prog/openssl-stuff/xip-io.crt'),
//}

const app = express();
const port:number = 3004;
const https_port:number=443;

app.listen(port, ()=>console.log("server is listening on port"+port));
//https.createServer(https_options, app).listen(https_port,()=>console.log("https running"));

enum SECURE{
    unspecified,
    secure,
    unsecure
}

function getMyUrl(req,secure:SECURE=SECURE.unspecified):string{
    let real_secure :boolean=false;
    switch(secure){
        case SECURE.secure:
            real_secure = true;
            break;
        case SECURE.unsecure:
            real_secure = false;
            break;
        case SECURE.unspecified:
        default:
            real_secure = req.secure;
    }
    return "http"+(real_secure?"s":"")+"://"+req.hostname+":"+(real_secure?https_port:port)+"/";
}

app.get('/', (req, res)=>{
    const url:string= getMyUrl(req);
    //res.type('application/xhtml+xml').sendFile(__dirname+'/pages/index.html');
    res.type('application/xhtml+xml').send(
`<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="de" xml:lang="de">
    <head>
        <title>Random Search</title>
        <link rel="search" type="application/opensearchdescription+xml"
            title="Random Search" href="`+url+`opensearch.xml" />
    </head>
    <body>
        <p>This is Random Search.</p>
        <p><a href="/opensearch.xml">Here</a> you can find our OpenSearch description<br/> 
        And <a href="/opensearch.xml?xmlonly=true">here</a> the version wich does not promt you to save it on disk</p> 
        <p>If you want to know what opensearch is try this eexample search:<br/><a href="/search?q=opensearch">`+url+`search?q=<b>opensearch</b></a></p>
        <p>Some browsers automatically detect the opensearch description from this site.<br/>
        This means that you can add this website as default search engine in the browser settings.<br/>
        In some modern Browser this does not seem to work.<br/>
        We believe this because this website has no HTTP<b>S</b> (secure HTTP).</p>
        <form action="search">
            <input type="text" name="q" id="q"/>
            <button type="submit">Suchen</button>
        </form>
        <br/>
        <p>
            <a href="`+getMyUrl(req,SECURE.unsecure)+`">http</a><br/>
            <a href="`+getMyUrl(req,SECURE.secure)+`">http<b>S</b></a> (not available at the moment)
        </p>
        <p>
        Search "<a href="/search?q=seten">seten</a>" to set the Language of some searchengines to English.<br/>
        Suche "<a href="/search?q=setde">setde</a>" um die Sprache einiger Suchmaschinen auf Deutsch zu stellen.<br/>
        Search "<a href="/search?q=reset">reset</a>" to reset the language settings.
        </p>
        <p>Current Language: `+langstr+`</p>
    </body>
</html>
`);
});

app.get('/favicon.ico', (req,res)=>{
    res.sendFile(__dirname+'/pict/favicon.ico');
});

app.get('/opensearch.xml',(req,res)=>{
    const url = getMyUrl(req);
    const opensearchdesc:string= `<?xml version="1.0" encoding="UTF-8"?>
<OpenSearchDescription 
xmlns="http://a9.com/-/spec/opensearch/1.1/" 
xmlns:moz="http://www.mozilla.org/2006/browser/search/">
<ShortName>Random search</ShortName>
<Description>A random searchengine for each query (`+url+`)</Description>
<Tags>random</Tags>
<Image height="16" width="16" type="image/vnd.microsoft.icon">`+url+`favicon.ico</Image>
<Url type="text/html" 
    template="`+url+`search?q={searchTerms}" />
<Url type="application/opensearchdescription+xml"
    rel="self"
    template="`+url+`opensearch.xml" />
<Query role="example" searchTerms="tach moin" />
<moz:SearchForm>`+url+`"</moz:SearchForm>
</OpenSearchDescription>
`;
    if(req.query.xmlonly)
        res.type('application/xml').send(opensearchdesc);
    else 
        res.type('application/opensearchdescription+xml').send(opensearchdesc);
})


app.get('/test', (req, res)=>{
    res.type('text/plain').send("Hallo\n");
})
app.get('/search', (req, res)=>{
    let query :string = req.query.q;

    if(query==='' || query === 'rs')
        res.redirect('/');
    if(query === 'setde')
        res.redirect('/set/lang/?lang=de');
    else if(query === 'seten')
        res.redirect('/set/lang/?lang=en');
    else if(query === 'reset')
        res.redirect('/set/lang/?lang=');
    else if(query === 'testrandom')
        res.redirect('/test/random/?n=100')
    else
        res.redirect(getRandomSearchEngine()+escape(query));
})

app.get('/test/random',(req,res)=>{
    let content:string= "";
    let n:number = 100;//0+req.query.n;//hack

    for(let i =0; i < n;++i){
        content += getRandomSearchEngine()+'\n';
    }
    res.type('text/plain').send(content);

});

app.get('/set/lang', (req, res)=>{
    let lang :string = req.query.lang;
    if(lang !== 'de' && lang !== 'en')
        lang = "";

    let langold = langstr;
    langstr = lang;
    res.type('text/plain').send("lang was: "+langold+"\nnow it is set to: "+langstr)
});

app.get('/get/lang',(req, res)=>{
    res.type('text/plain').send('lang is: '+langstr);
});

//const searchEngines :string[] = [
//    "https://www.google.de/search?q=",
//    "https://www.duckduckgo.com/?hl=de-de&q=",
//    "https://www.bing.com/search?q="
//]


function google():string{
    let base :string = "https://www.google.de/search?";
    let suffix :string ="q="
    if(langstr === "de")
        return base +"hl=de-de&"+suffix;
    else if(langstr === 'en')
        return base +"hl=en-us&"+suffix;
    else
        return base+"q=";
}
function duck():string{
    return "https://www.duckduckgo.com/?q=";
}
function bing ():string{
    return "https://www.bing.com/search?q="
}
function ecosia():string{
    return "https://www.ecosia.org/search?q="
}

interface searchFuncProb{
    func: ()=>string;
    prob:number;
    probWhenLast:number;
}
const searchFunctions: searchFuncProb[]= [
    {func: duck, prob: 8, probWhenLast:2},
    {func: google, prob: 6, probWhenLast:1},
    {func: ecosia, prob:5, probWhenLast:1},
    {func: bing, prob:4, probWhenLast:1}
]

const prob_sum:number = function():number{
    let sum =0;
    for(let funcProb of searchFunctions){
        sum += funcProb.prob;
    }
    return sum;
}();

let lastFuncProb:searchFuncProb={func:()=>"",prob:0,probWhenLast:0}
let divisor:number=1.0;
function getRandomSearchEngine():string{
    const max = prob_sum - lastFuncProb.prob + lastFuncProb.probWhenLast/divisor;

    const rand = Math.random()*max;
    
    let upper_bound = 0;
    let funcProb:searchFuncProb|null=null;
    for(let fP of searchFunctions){
        upper_bound += (fP === lastFuncProb)? fP.probWhenLast/divisor : fP.prob;
        if(rand <= upper_bound) {
            funcProb = fP;
            break;
        }
    }
    if(!funcProb)
        return '/error/?q='
    
    if(lastFuncProb==funcProb)
        divisor*=1.5;
    else
        divisor=1;

    lastFuncProb = funcProb;

    return funcProb.func();
}
