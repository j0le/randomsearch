randomsearch
------------

randomsearch uses http redirects to forward your search request to a random searchengine

supported engines:
- duckduckgo
- google
- bing

you can add your own engines in the code and change the probability for each engine.

Use randomsearch
----------------

To use randomsearch
- install nodejs (and nmp)
- clone the project
- run `nmp install` in the repository
- run `nmp run start` to compile and run the server
- open localhost:3004 in the browser
- modern browsers are able to detect the search abilities, so you can add this s.engine ti your browser like the youtube search for example.

you should autostart randomsearch on startup of your pc.
how this can be done (without using `npm run ...` and without calling tsc) will be explained soon.